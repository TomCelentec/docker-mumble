# Docker Mumble

## Description

This repository contains the Dockerfile and configuration needed to set up a Mumble server in a Docker container.

## Prerequisites

- Docker installed on your machine.

## Usage

1. Clone this repository:
   ```bash
   git clone https://gitlab.com/TomCelentec/docker-mumble.git
   ```
2. Navigate to the cloned repository:
   ```bash
   cd docker-mumble
   ```
3. Build the Docker image:
   ```bash
   docker build -t mumble-server .
   ```
4. Run the Docker container:
   ```bash
   docker run -d -p 64738:64738 mumble-server
   ```

## License
This project is licensed under the MIT License. See [LICENSE](LICENSE) for more information.
