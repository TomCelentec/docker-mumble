FROM ubuntu:latest
MAINTAINER Clément COTE <tom.celentec@protonmail.com>

ENV TZ=Europe/Paris

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y mumble-server

# RUN mkdir config && \
#     mv /var/lib/mumble-server/mumble-server.sqlite config/ && \
#     ln -s config/mumble-server.sqlite /var/lib/mumble-server/mumble-server.sqlite && \
#     mv /etc/mumble-server.ini config/ && \
#     ln -s config/mumble-server.ini /etc/mumble-server.ini

RUN mkdir /home/config && \
    ln -s /home/config/mumble-server.sqlite /var/lib/mumble-server/mumble-server.sqlite && \
    mv /etc/mumble/mumble-server.ini /home/config/ && \
    ln -s /home/config/mumble-server.ini /etc/mumble/mumble-server.ini


EXPOSE 64738/udp
EXPOSE 64738/tcp

ENTRYPOINT ["/usr/bin/mumble-server", "-fg"]
